const dbconnection = require('../database/dbconnection');

module.exports = {
    async index(req, res) {
        const ong_id = req.headers.authorization;

        const incidents = await dbconnection('incidents')
            .where('ong_id', ong_id)
            .select('*');

        return res.json(incidents);
    }
};