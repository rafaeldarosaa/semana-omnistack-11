const dbconnection = require('../database/dbconnection');

module.exports = {
    async create(req, res) {
        const { id } = req.body;

        const ong = await dbconnection('ongs')
            .where('id', id)
            .select('name')
            .first();  //retorna 1 resultado em vez de 1 array

        if(!ong) {
            return res.status(400).json({ error: 'No ONG found with this ID' });
        }

        return res.json(ong);
    }
};